package com.polyscript.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class PolyscriptApplication {

    public static void main(String[] args) {
         SpringApplication.run(PolyscriptApplication.class, args);
    }

}
