package com.example.demo;

import org.apache.commons.lang.ObjectUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;



@SpringBootApplication
@EnableEurekaServer  /*"side server", fournie par l'api spring-cloud pour faciliter la mise en place de la découverte du service Eureka*/
public class DemoApplication {


   public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }




}


